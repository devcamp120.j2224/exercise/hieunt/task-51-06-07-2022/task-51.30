package com.devcamp.j02_javabasic.s10;

public class Customer {
    byte myByte; // 1 byte whole num -128 to 127
    short myShortNum; // 2 bytes whole num -32,768 to 32,767
    int myIntNum; // 4 bytes integer (whole number)
    long myLongNum; // 8 bytes
    float myFloatNum; // 4 bytes 6 to 7 decimal digits (floating point number)
    double myDoubleNum; // 8 bytes 15 decimal digits
    boolean myBool; // 1 bit boolean
    char myLetter; // 2 bytes character

    //khởi tạo không có tham số
    public Customer() {
        myByte = 1; 
        myShortNum = 11;
        myIntNum = 111;
        myLongNum = 1111;
        myFloatNum = 11.11f; 
        myDoubleNum = 101.101d;
        myBool = true; 
        myLetter = 'A'; 
    }
    // khởi tạo có tất cả 8 tham số
    public Customer(byte byteNum, short shortNum, int intNum, long longNum,
                    float floatNum, double doubleNum, boolean bool, char letter) {
        myByte = byteNum;
        myShortNum = shortNum;
        myIntNum = intNum;
        myLongNum = longNum;
        myFloatNum = floatNum;
        myDoubleNum = doubleNum;
        myBool = bool;
        myLetter = letter;
    }

    public static void main(String[] args) throws Exception {
        System.out.println("Run 1");
        Customer customer = new Customer();
        customer.showInfo();

        System.out.println("Run 2");
        byte myByte = 2;
        short myShortNum = 22;
        customer = new Customer(myByte, myShortNum, 222, 2222, 22.22f, 202.202d, false, 'B');
        customer.showInfo();

        System.out.println("Run 3");
        myByte = 3;
        myShortNum = 33;
        customer = new Customer(myByte, myShortNum, 333, 3333, 33.33f, 303.303d, true, 'C');
        customer.showInfo();

        System.out.println("Run 4");
        myByte = 4;
        myShortNum = 44;
        customer = new Customer(myByte, myShortNum, 444, 4444, 44.44f, 404.404d, false, 'D');
        customer.showInfo();

        System.out.println("Run 5");
        myByte = 5;
        myShortNum = 55;
        customer = new Customer(myByte, myShortNum, 555, 5555, 55.55f, 505.505, true, 'E');
        customer.showInfo();
    }
    public void showInfo() {
        System.out.printf("myByte=" + this.myByte); 
        System.out.printf(", myShortNum=" + this.myShortNum);
        System.out.printf(", myIntNum=" + this.myIntNum);
        System.out.printf(", myLongNum=" + this.myLongNum);
        System.out.printf(", myFloatNum=" + this.myFloatNum); 
        System.out.printf(", myDoubleNum=" + this.myDoubleNum);
        System.out.printf(", myBool=" + this.myBool); 
        System.out.println(", myLetter=" + this.myLetter); 
    }
}
